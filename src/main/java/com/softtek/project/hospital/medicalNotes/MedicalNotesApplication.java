package com.softtek.project.hospital.medicalNotes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class MedicalNotesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedicalNotesApplication.class, args);
	}

}
