package com.softtek.project.hospital.medicalNotes.controller;

import com.softtek.project.hospital.medicalNotes.model.PrescriptionDetail;
import com.softtek.project.hospital.medicalNotes.service.PrescriptionDetailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="hospital/medicalNotes")
public class PrescriptionDetailController {


    @Autowired
    PrescriptionDetailServiceImpl prescriptionDetailServiceImpl;



    @GetMapping(value = "/medicalPrescriptions/{idMedicalPrescription}/medicalPrescriptionDetails",produces = {"application/json"})
    public List<PrescriptionDetail> prescriptionsDetailByMedicalPrescriptionId
            (@PathVariable("idMedicalPrescription") Integer id){
        return (List<PrescriptionDetail>) prescriptionDetailServiceImpl.findByMedicalPrescription(id);
    }

    @PutMapping(value = "/medicalPrescriptionDetail",produces = {"application/json"})
    public PrescriptionDetail updateDischargeNote(@RequestBody PrescriptionDetail prescriptionDetail){
        prescriptionDetailServiceImpl.updatePrescription(prescriptionDetail);
        return prescriptionDetail;
    }


}
