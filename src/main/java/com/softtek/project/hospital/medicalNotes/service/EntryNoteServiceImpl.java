package com.softtek.project.hospital.medicalNotes.service;

import com.softtek.project.hospital.medicalNotes.exception.NoteNotFoundException;
import com.softtek.project.hospital.medicalNotes.model.EntryNote;
import com.softtek.project.hospital.medicalNotes.repository.EntryNoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class EntryNoteServiceImpl implements EntryNoteService {

    @Autowired
    EntryNoteRepository entryNoteRepository;

    @Override
    public EntryNote findById(Integer id) {
        Optional<EntryNote> entryNote;
        entryNote = entryNoteRepository.findById(id);

        if (entryNote.isPresent()){
            return entryNote.get();
        }else{
            throw new NoteNotFoundException();
        }
    }

    @Override
    public Iterable<EntryNote> findByEmployee(Integer idEmployee) {
        Iterable<EntryNote> notes = entryNoteRepository.findAll();
        ArrayList<EntryNote> findNotes = new ArrayList<>();

        for (EntryNote note:notes){
            if(note.getEmployee().compareTo(idEmployee) == 0){
                findNotes.add(note);
            }
        }

        return findNotes;
    }

    @Override
    public Iterable<EntryNote> findByHealthRecord(Integer idHealthRecord) {
        Iterable<EntryNote> notes = entryNoteRepository.findAll();
        ArrayList<EntryNote> findNotes = new ArrayList<>();

        for (EntryNote note:notes){
            if(note.getHealthRecord().compareTo(idHealthRecord) == 0){
                findNotes.add(note);
            }
        }

        return findNotes;
    }

    @Override
    public boolean delete(int id) {
        if(entryNoteRepository.existsById(id)){
            entryNoteRepository.deleteById(id);
            return true;
        }else{
            throw new NoteNotFoundException();
        }
    }

    @Override
    public EntryNote createEntryNote(EntryNote entryNote) {
        return  entryNoteRepository.save(entryNote);
    }

    @Override
    public EntryNote updateEntryNote(EntryNote entryNote) {
        return entryNoteRepository.save(entryNote);
    }
}
