package com.softtek.project.hospital.medicalNotes.controller;

import com.softtek.project.hospital.medicalNotes.model.EntryNote;
import com.softtek.project.hospital.medicalNotes.repository.EntryNoteRepository;
import com.softtek.project.hospital.medicalNotes.service.EntryNoteServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping(value = "hospital/medicalNotes")
public class EntryNoteController {

    @Autowired
    EntryNoteServiceImpl entryNoteServiceImpl;


    @GetMapping(value = "/entryNotes/{idEntryNote}",produces = {"application/json"})
    public EntryNote entryNote(@PathVariable("idEntryNote") Integer id){
           return entryNoteServiceImpl.findById(id);
        }

    @DeleteMapping(value = "/entryNotes/{idEntryNote}",produces = {"application/json"})
    public ResponseEntity deleteEntryNote(@PathVariable("idEntryNote") Integer id){
        if(entryNoteServiceImpl.delete(id)) {
            return new ResponseEntity(
                    "Note deleted",
                    HttpStatus.OK
            );
        }else{
            return new ResponseEntity(
                    "Note not found",
                    HttpStatus.NOT_FOUND
            );
        }
    }

    @PostMapping(value = "/entryNotes",produces = {"application/json"})
    public EntryNote createEntryNote(@RequestBody EntryNote entryNote){
        entryNoteServiceImpl.createEntryNote(entryNote);
        return entryNote;
    }

    @PutMapping(value = "/entryNotes",produces = {"application/json"})
    public EntryNote updateEntryNote(@RequestBody EntryNote entryNote){
        entryNoteServiceImpl.updateEntryNote(entryNote);
        return entryNote;
    }

    @GetMapping(value = "/healthRecords/{idHealthRecord}/entryNotes",produces = {"application/json"})
    public List<EntryNote> entryNoteByHealthRecord(@PathVariable("idHealthRecord") Integer id){
        return (List<EntryNote>) entryNoteServiceImpl.findByHealthRecord(id);
    }

    @GetMapping(value = "/employees/{idEmployee}/entryNotes",produces = {"application/json"})
    public List<EntryNote> entryNoteByEmloyee(@PathVariable("idEmployee") Integer id){
        return (List<EntryNote>) entryNoteServiceImpl.findByEmployee(id);
    }

}
