package com.softtek.project.hospital.medicalNotes.repository;

import com.softtek.project.hospital.medicalNotes.model.EntryNote;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EntryNoteRepository extends CrudRepository<EntryNote,Integer>  {
}
