package com.softtek.project.hospital.medicalNotes.service;

import com.softtek.project.hospital.medicalNotes.model.DischargeNote;

public interface DischargeNoteService {

    DischargeNote findById(Integer id);

    Iterable<DischargeNote> findByEmployee(Integer idEmployee);

    Iterable<DischargeNote> findByHealthRecord(Integer idHealthRecord);

    boolean delete(int id);

    DischargeNote createDischargeNote(DischargeNote dischargeNote);

    DischargeNote updateDischargeNote(DischargeNote dischargeNote);
}