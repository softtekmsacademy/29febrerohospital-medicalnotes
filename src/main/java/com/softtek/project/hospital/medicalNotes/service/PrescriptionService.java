package com.softtek.project.hospital.medicalNotes.service;

import com.softtek.project.hospital.medicalNotes.model.MedicalNote;
import com.softtek.project.hospital.medicalNotes.model.Prescription;

import java.util.List;

public interface PrescriptionService {

    Prescription findByIdDischargeNote(Integer idDischargeNote);

    Prescription findByIdEntryNote(Integer idEntryNote);

    Iterable<Prescription> findByHealthRecord(Integer idHealthRecord);

    Prescription updatePrescription(Prescription prescription);
}
