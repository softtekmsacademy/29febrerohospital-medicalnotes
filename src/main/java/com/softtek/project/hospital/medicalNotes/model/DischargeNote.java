package com.softtek.project.hospital.medicalNotes.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "discharge_notes")
public class DischargeNote extends MedicalNote{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idDischargeNote;
    private String dischargeReason;

    public DischargeNote() {}

    public DischargeNote(LocalDateTime timestamp, Integer healthRecord, String prognosis, String diagnosis, VitalSigns vitalSigns, Prescription prescription, Integer employee, Integer idDischargeNote, Integer idVitalSigns, String dischargeReason) {
        super(timestamp, healthRecord, prognosis, diagnosis, vitalSigns, prescription, employee);
        this.idDischargeNote = idDischargeNote;
        this.dischargeReason = dischargeReason;
    }

    public Integer getIdDischargeNote() {
        return idDischargeNote;
    }

    public void setIdDischargeNote(Integer idDischargeNote) {
        this.idDischargeNote = idDischargeNote;
    }




    public String getDischargeReason() {
        return dischargeReason;
    }

    public void setDischargeReason(String dischargeReason) {
        this.dischargeReason = dischargeReason;
    }
}
