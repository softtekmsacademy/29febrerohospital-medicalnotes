package com.softtek.project.hospital.medicalNotes.controller;


import com.softtek.project.hospital.medicalNotes.model.VitalSigns;
import com.softtek.project.hospital.medicalNotes.service.VitalSignServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "hospital/medicalNotes")
public class VitalSignController {

    @Autowired
    VitalSignServiceImpl vitalSignService;

    @GetMapping(value = "/entryNotes/{idEntryNote}/vitalSigns")
    public VitalSigns getByEntryNote(@PathVariable("idEntryNote") Integer id){
        return  vitalSignService.getByEntryNote(id);
    }

    @GetMapping(value = "/dischargeNotes/{idDischargeNote}/vitalSigns")
    public VitalSigns getByDischargeNote(@PathVariable("idDischargeNote") Integer id){
        return  vitalSignService.getByDischargeNote(id);
    }

    @PutMapping(value = "/vitalSigns",produces = {"application/json"})
    public VitalSigns updateVitalSigns(@RequestBody VitalSigns vitalSigns){
        vitalSignService.updateVitalSigns(vitalSigns);
        return vitalSigns;
    }

    @GetMapping(value = "/healthRecords/{idHealthRecord}/vitalSigns",produces = {"application/json"})
    public List<VitalSigns> vitalSignsByHealthRecord(@PathVariable("idHealthRecord") Integer id){
        return (List<VitalSigns>) vitalSignService.findByHealthRecord(id);
    }
}
