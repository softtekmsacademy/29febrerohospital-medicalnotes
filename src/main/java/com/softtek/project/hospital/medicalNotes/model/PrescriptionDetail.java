package com.softtek.project.hospital.medicalNotes.model;

import javax.persistence.*;

@Entity
@Table(name = "medical_prescription_detail")
public class PrescriptionDetail {

    public PrescriptionDetail(){}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idMedicalPrescriptionDetail;
    private String dose;
    private String administrationRoute;
    private String periodicity;

    @Column(name = "id_product")
    private Integer product;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "id_medical_prescription", insertable = false, updatable = false )
    private Prescription prescription;

    public PrescriptionDetail(String dose, String administrationRoute, String periodicity, Integer product) {
        this.dose = dose;
        this.administrationRoute = administrationRoute;
        this.periodicity = periodicity;
        this.product = product;
    }

    public Integer getIdMedicalPrescriptionDetail() {
        return idMedicalPrescriptionDetail;
    }

    public void setIdMedicalPrescriptionDetail(Integer idMedicalPrescriptionDetail) {
        this.idMedicalPrescriptionDetail = idMedicalPrescriptionDetail;
    }

    public String getdose() {
        return dose;
    }

    public void setdose(String dose) {
        this.dose = dose;
    }

    public String getadministrationRoute() {
        return administrationRoute;
    }

    public void setadministrationRoute(String administrationRoute) {
        this.administrationRoute = administrationRoute;
    }

    public String getperiodicity() {
        return periodicity;
    }

    public Integer getProduct() {
        return product;
    }

    public void setperiodicity(String periodicity) {
        this.periodicity = periodicity;
    }

}
