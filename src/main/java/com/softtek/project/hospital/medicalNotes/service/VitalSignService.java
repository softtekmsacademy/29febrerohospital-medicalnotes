package com.softtek.project.hospital.medicalNotes.service;

import com.softtek.project.hospital.medicalNotes.model.VitalSigns;

public interface VitalSignService {

    VitalSigns getByDischargeNote(Integer id);

    VitalSigns getByEntryNote(Integer id);

    VitalSigns updateVitalSigns(VitalSigns vitalSigns);

    Iterable<VitalSigns> findByHealthRecord(Integer idHealthRecord);

}
