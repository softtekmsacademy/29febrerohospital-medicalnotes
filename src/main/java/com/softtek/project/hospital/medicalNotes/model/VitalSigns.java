package com.softtek.project.hospital.medicalNotes.model;

import javax.persistence.*;

@Entity
public class VitalSigns {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idVitalSigns;
    private Integer heartRate;
    private Double bodyTemperature;
    private Double bloodPressure;
    private Double meanArterialPressure;
    private Integer respiratoryRate;
    private Float bloodOxygen;

    public VitalSigns(){}

    public VitalSigns(Integer heartRate, Double bodyTemperature, Double bloodPressure, Double meanArterialPressure, Integer respiratoryRate, Float bloodOxygen) {
        this.heartRate = heartRate;
        this.bodyTemperature = bodyTemperature;
        this.bloodPressure = bloodPressure;
        this.meanArterialPressure = meanArterialPressure;
        this.respiratoryRate = respiratoryRate;
        this.bloodOxygen = bloodOxygen;
    }

    public Integer getIdVitalSigns() {
        return idVitalSigns;
    }

    public Integer getheartRate() {
        return heartRate;
    }

    public void setheartRate(Integer heartRate) {
        this.heartRate = heartRate;
    }

    public Double getbodyTemperature() {
        return bodyTemperature;
    }

    public void setbodyTemperature(Double bodyTemperature) {
        this.bodyTemperature = bodyTemperature;
    }

    public Double getbloodPressure() {
        return bloodPressure;
    }

    public void setbloodPressure(Double bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public Double getmeanArterialPressure() {
        return meanArterialPressure;
    }

    public void setmeanArterialPressure(Double meanArterialPressure) {
        this.meanArterialPressure = meanArterialPressure;
    }

    public Integer getrespiratoryRate() {
        return respiratoryRate;
    }

    public void setrespiratoryRate(Integer respiratoryRate) {
        this.respiratoryRate = respiratoryRate;
    }

    public Float getbloodOxygen() {
        return bloodOxygen;
    }

    public void setbloodOxygen(Float bloodOxygen) {
        this.bloodOxygen = bloodOxygen;
    }
}
