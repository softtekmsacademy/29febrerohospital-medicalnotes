package com.softtek.project.hospital.medicalNotes.service;

import com.softtek.project.hospital.medicalNotes.exception.NoteNotFoundException;
import com.softtek.project.hospital.medicalNotes.repository.DischargeNoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.softtek.project.hospital.medicalNotes.model.DischargeNote;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class DischargeNoteServiceImpl implements DischargeNoteService {

    @Autowired
    DischargeNoteRepository dischargeNoteRepository;

    @Override
    public DischargeNote findById(Integer id) {
        Optional<DischargeNote> dischargeNote;
        dischargeNote = dischargeNoteRepository.findById(id);

        if (dischargeNote.isPresent()){
            return dischargeNote.get();
        }else{
            throw new NoteNotFoundException();
        }
    }

    @Override
    public Iterable<DischargeNote> findByEmployee(Integer idEmployee) {
        Iterable<DischargeNote> notes = dischargeNoteRepository.findAll();
        ArrayList<DischargeNote> findNotes = new ArrayList<>();

        for (DischargeNote note:notes){
            if(note.getEmployee().compareTo(idEmployee) == 0){
                findNotes.add(note);
            }
        }

        return findNotes;
    }

    @Override
    public Iterable<DischargeNote> findByHealthRecord(Integer idHealthRecord) {
        Iterable<DischargeNote> notes = dischargeNoteRepository.findAll();
        ArrayList<DischargeNote> findNotes = new ArrayList<>();

        for (DischargeNote note:notes){
            if(note.getHealthRecord().compareTo(idHealthRecord) == 0){
                findNotes.add(note);
            }
        }

        return findNotes;
    }

    @Override
    public boolean delete(int id) {
        if(dischargeNoteRepository.existsById(id)){
            dischargeNoteRepository.deleteById(id);
            return true;
        }else{
            throw new NoteNotFoundException();
        }
    }

    @Override
    public DischargeNote createDischargeNote(DischargeNote dischargeNote) {
        return  dischargeNoteRepository.save(dischargeNote);
    }

    @Override
    public DischargeNote updateDischargeNote(DischargeNote dischargeNote) {
        return dischargeNoteRepository.save(dischargeNote);
    }
}
