package com.softtek.project.hospital.medicalNotes.model;


import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "entry_note")
public class EntryNote extends MedicalNote {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idEntryNote;

    private String entryReason;
    public EntryNote(){}

    public EntryNote(LocalDateTime timestamp, Integer healthRecord, String prognosis, String diagnosis, VitalSigns vitalSigns, Prescription prescription, Integer employee, String entryReason) {
        super(timestamp, healthRecord, prognosis, diagnosis, vitalSigns, prescription, employee);
        this.entryReason = entryReason;
    }

    public Integer getId_entry_note() {
        return idEntryNote;
    }

    public String getentryReason() {
        return entryReason;
    }
    public void setentryReason(String entryReason) {
        this.entryReason = entryReason;
    }
}
