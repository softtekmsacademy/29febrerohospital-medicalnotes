package com.softtek.project.hospital.medicalNotes.controller;

import com.softtek.project.hospital.medicalNotes.model.DischargeNote;
import com.softtek.project.hospital.medicalNotes.service.DischargeNoteServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;


@RestController
@RequestMapping(path="hospital/medicalNotes")
public class DischargeNotesController {

    @Autowired
    DischargeNoteServiceImpl dischargeNoteServiceImpl;


    @GetMapping(value = "/dischargeNotes/{idDischargeNote}",produces = {"application/json"})
    public DischargeNote dischargeNote(@PathVariable("idDischargeNote") Integer id){
        return dischargeNoteServiceImpl.findById(id);
    }

    @DeleteMapping(value = "/dischargeNotes/{idDischargeNote}",produces = {"application/json"})
    public ResponseEntity<String> deleteDischargeNote(@PathVariable("idDischargeNote") Integer id){
        if(dischargeNoteServiceImpl.delete(id)){
            return new ResponseEntity(
                    "Note deleted",
              HttpStatus.OK
            );
        }else{
            return new ResponseEntity(
                    "Note not found",
                    HttpStatus.NOT_FOUND
            );
        }
    }

    @PostMapping(value = "/dischargeNotes",produces = {"application/json"})
    public DischargeNote createDischargeNote(@RequestBody DischargeNote dischargeNote){
        dischargeNoteServiceImpl.createDischargeNote(dischargeNote);
        return dischargeNote;
    }

    @PutMapping(value = "/dischargeNotes",produces = {"application/json"})
    public DischargeNote updateDischargeNote(@RequestBody DischargeNote dischargeNote){
        dischargeNoteServiceImpl.updateDischargeNote(dischargeNote);
        return dischargeNote;
    }

    @GetMapping(value = "/healthRecords/{idHealthRecord}/dischargeNotes",produces = {"application/json"})
    public List<DischargeNote> dischargeNoteByHealthRecord(@PathVariable("idHealthRecord") Integer id){
        return (List<DischargeNote>) dischargeNoteServiceImpl.findByHealthRecord(id);
    }

    @GetMapping(value = "/employees/{idEmployee}/dischargeNotes",produces = {"application/json"})
    public List<DischargeNote> dischargeNoteByEmployee(@PathVariable("idEmployee") Integer id){
        return (List<DischargeNote>) dischargeNoteServiceImpl.findByEmployee(id);
    }

}