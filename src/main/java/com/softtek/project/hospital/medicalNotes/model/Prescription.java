package com.softtek.project.hospital.medicalNotes.model;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "medical_prescription")
public class Prescription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idMedicalPrescription;
    @CreationTimestamp
    private LocalDateTime timestamp;

    @OneToMany(cascade = {CascadeType.ALL})
    @JoinColumn(name = "id_medical_prescription") // we need to duplicate the physical information
    private Set<PrescriptionDetail> details;

    public Prescription(){}

    public Prescription(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getId_medical_prescription() {
        return idMedicalPrescription;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public Set<PrescriptionDetail> getDetails() {
        return details;
    }
}
