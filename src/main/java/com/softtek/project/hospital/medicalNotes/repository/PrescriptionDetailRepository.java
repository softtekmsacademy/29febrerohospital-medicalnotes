package com.softtek.project.hospital.medicalNotes.repository;

import com.softtek.project.hospital.medicalNotes.model.PrescriptionDetail;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrescriptionDetailRepository extends CrudRepository<PrescriptionDetail,Integer> {
}

