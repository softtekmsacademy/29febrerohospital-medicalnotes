package com.softtek.project.hospital.medicalNotes.model;

import javax.persistence.*;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;


@MappedSuperclass
public class MedicalNote {

    @Column(name="creation_date")
    @CreationTimestamp
    private LocalDateTime creationDate;

    private String prognosis;
    private String diagnosis;


    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "id_vital_signs", referencedColumnName = "idVitalSigns")
    private VitalSigns vitalSigns;

    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "id_medical_prescription", referencedColumnName = "idMedicalPrescription")
    private Prescription prescription;

    @Column(name = "id_employee")
    private Integer employee;

    @Column(name = "id_health_record")
    private Integer healthRecord;

    public MedicalNote(){

    }

    public MedicalNote(LocalDateTime creationDate, Integer healthRecord, String prognosis, String diagnosis, VitalSigns vitalSigns, Prescription prescription, Integer employee) {
        this.creationDate = creationDate;
        this.prognosis = prognosis;
        this.diagnosis = diagnosis;
        this.healthRecord = healthRecord;
        this.vitalSigns = vitalSigns;
        this.prescription = prescription;
        this.employee = employee;
    }

    public LocalDateTime getTimestamp() {
        return creationDate;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public String getPrognosis() {
        return prognosis;
    }

    public void setPrognosis(String prognosis) {
        this.prognosis = prognosis;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public VitalSigns getVitalSigns() {
        return vitalSigns;
    }

    public void setVitalSigns(VitalSigns vitalSigns) {
        this.vitalSigns = vitalSigns;
    }

    public Prescription getPrescription() {
        return prescription;
    }

    public void setPrescription(Prescription prescription) {
        this.prescription = prescription;
    }

    public Integer getEmployee() {
        return employee;
    }

    public void setEmployee(Integer employee) {
        this.employee = employee;
    }

    public Integer getHealthRecord() {
        return healthRecord;
    }

    public void setHealthRecord(Integer healthRecord) {
        this.healthRecord = healthRecord;
    }
}
