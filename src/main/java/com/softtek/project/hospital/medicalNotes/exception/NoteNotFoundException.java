package com.softtek.project.hospital.medicalNotes.exception;


public class NoteNotFoundException extends RuntimeException{
    private static final long serialVersionUID = 1L;
}
