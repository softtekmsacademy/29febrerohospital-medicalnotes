package com.softtek.project.hospital.medicalNotes.exception;

public class VitalSignNotFoundException extends RuntimeException{
    private static final long serialVersionUID = 1L;
}
