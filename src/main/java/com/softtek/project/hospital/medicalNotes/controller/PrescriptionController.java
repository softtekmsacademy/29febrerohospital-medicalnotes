package com.softtek.project.hospital.medicalNotes.controller;


import com.softtek.project.hospital.medicalNotes.service.PrescriptionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.softtek.project.hospital.medicalNotes.model.Prescription;

import java.util.List;

@RestController
@RequestMapping(path="hospital/medicalNotes")
public class PrescriptionController {

    @Autowired
    PrescriptionServiceImpl prescriptionServiceImpl;



    @GetMapping(value = "/healthRecords/{idHealthRecord}/prescriptions",produces = {"application/json"})
    public List<Prescription> prescriptionsByHealthRecord(@PathVariable("idHealthRecord") Integer id){
        return (List<Prescription>) prescriptionServiceImpl.findByHealthRecord(id);
    }

    @GetMapping(value = "/dischargeNotes/{idDischargeNote}/medicalPrescriptions", produces = {"application/json"})
    public Prescription prescriptionByDischargeNoteID(@PathVariable("idDischargeNote") Integer id) {
        return prescriptionServiceImpl.findByIdDischargeNote(id);
    }

    @GetMapping(value = "/entryNotes/{idEntryNote}/medicalPrescriptions", produces = {"application/json"})
    public Prescription prescriptionByEntryNoteID(@PathVariable("idEntryNote") Integer id) {
        return prescriptionServiceImpl.findByIdEntryNote(id);
    }

    @PutMapping(value = "/medicalPrescription",produces = {"application/json"})
    public Prescription updateDischargeNote(@RequestBody Prescription prescription){
        prescriptionServiceImpl.updatePrescription(prescription);
        return prescription;
    }
}