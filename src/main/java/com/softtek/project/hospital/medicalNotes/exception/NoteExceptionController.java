package com.softtek.project.hospital.medicalNotes.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;

@ControllerAdvice
public class NoteExceptionController {

    @ExceptionHandler(value = NoteNotFoundException.class)
    public ResponseEntity<Object> exception(NoteNotFoundException exception) {
        HashMap<String,String> errorMsg = new HashMap<>();
            errorMsg.put("Error","Note ID not found");
            errorMsg.put("Status", HttpStatus.NOT_FOUND.toString());
        return new ResponseEntity<>(errorMsg, HttpStatus.NOT_FOUND);
    }
}
