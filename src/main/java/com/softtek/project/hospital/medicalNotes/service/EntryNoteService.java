package com.softtek.project.hospital.medicalNotes.service;

import com.softtek.project.hospital.medicalNotes.model.EntryNote;


public interface EntryNoteService {

    EntryNote findById(Integer id);

    Iterable<EntryNote> findByEmployee(Integer idEmployee);

    Iterable<EntryNote> findByHealthRecord(Integer idHealthRecord);

    boolean delete(int id);

    EntryNote createEntryNote(EntryNote entryNote);

    EntryNote updateEntryNote(EntryNote entryNote);
}
