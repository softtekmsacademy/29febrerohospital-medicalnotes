package com.softtek.project.hospital.medicalNotes.repository;

import com.softtek.project.hospital.medicalNotes.model.DischargeNote;
import org.springframework.data.repository.CrudRepository;
import com.softtek.project.hospital.medicalNotes.model.VitalSigns;

public interface VitalSignsRepository extends CrudRepository<VitalSigns, Integer> {

}
