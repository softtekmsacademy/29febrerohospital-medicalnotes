package com.softtek.project.hospital.medicalNotes.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.softtek.project.hospital.medicalNotes.model.Prescription;

@Repository
public interface PrescriptionRepository extends CrudRepository<Prescription,Integer> {
}
