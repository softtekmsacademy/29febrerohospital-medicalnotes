package com.softtek.project.hospital.medicalNotes.service;

import com.softtek.project.hospital.medicalNotes.exception.VitalSignNotFoundException;
import com.softtek.project.hospital.medicalNotes.model.DischargeNote;
import com.softtek.project.hospital.medicalNotes.model.EntryNote;
import com.softtek.project.hospital.medicalNotes.model.VitalSigns;
import com.softtek.project.hospital.medicalNotes.repository.VitalSignsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class VitalSignServiceImpl implements VitalSignService {

    @Autowired
    VitalSignsRepository vitalSignsRepository;

    @Autowired
    EntryNoteServiceImpl entryNoteService;

    @Autowired
    DischargeNoteServiceImpl dischargeNoteService;

    @Override
    public VitalSigns getByDischargeNote(Integer id) {
        return dischargeNoteService.findById(id).getVitalSigns();
    }

    @Override
    public VitalSigns getByEntryNote(Integer id) {
        return entryNoteService.findById(id).getVitalSigns();
    }

    @Override
    public VitalSigns updateVitalSigns(VitalSigns vitalSigns) {
        if(vitalSignsRepository.findById(vitalSigns.getIdVitalSigns()).isPresent()){
            return vitalSignsRepository.save(vitalSigns);
        }else{
            throw new VitalSignNotFoundException();
        }
    }

    @Override
    public Iterable<VitalSigns> findByHealthRecord(Integer idHealthRecord) {
        Iterable<EntryNote> entryNote = entryNoteService.findByHealthRecord(idHealthRecord);
        Iterable<DischargeNote> dischargeNotes = dischargeNoteService.findByHealthRecord(idHealthRecord);
        ArrayList<VitalSigns> findVitalSigns = new ArrayList<>();

        entryNote.forEach((x)-> findVitalSigns.add(x.getVitalSigns()));
        dischargeNotes.forEach((x)-> findVitalSigns.add(x.getVitalSigns())) ;
        return findVitalSigns;
    }
}
