package com.softtek.project.hospital.medicalNotes.service;

import com.softtek.project.hospital.medicalNotes.model.PrescriptionDetail;

public interface PrescriptionDetailService {

    Iterable<PrescriptionDetail> findByMedicalPrescription(Integer idMedicalPrescription);

    PrescriptionDetail updatePrescription(PrescriptionDetail prescriptionDetail);
}
