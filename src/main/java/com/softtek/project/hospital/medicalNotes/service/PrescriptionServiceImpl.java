package com.softtek.project.hospital.medicalNotes.service;

import com.softtek.project.hospital.medicalNotes.exception.NoteNotFoundException;
import com.softtek.project.hospital.medicalNotes.model.DischargeNote;
import com.softtek.project.hospital.medicalNotes.model.EntryNote;
import com.softtek.project.hospital.medicalNotes.repository.EntryNoteRepository;
import com.softtek.project.hospital.medicalNotes.repository.PrescriptionRepository;
import com.softtek.project.hospital.medicalNotes.repository.DischargeNoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.softtek.project.hospital.medicalNotes.model.Prescription;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PrescriptionServiceImpl implements PrescriptionService{

    @Autowired
    PrescriptionRepository prescriptionRepository;
    @Autowired
    EntryNoteServiceImpl entryNoteService;
    @Autowired
    DischargeNoteServiceImpl dischargeNoteService;

    @Override
    public Prescription findByIdDischargeNote(Integer id) {
        Optional<Prescription> prescription;
        prescription = prescriptionRepository.findById(id);

        if (prescription.isPresent()){
            return prescription.get();
        }else{
            throw new NoteNotFoundException();
        }
    }

    @Override
    public Prescription findByIdEntryNote(Integer id) {
        Optional<Prescription> prescription;
        prescription = prescriptionRepository.findById(id);

        if (prescription.isPresent()){
            return prescription.get();
        }else{
            throw new NoteNotFoundException();
        }
    }

    @Override
   public Iterable<Prescription> findByHealthRecord(Integer idHealthRecord) {
              Iterable<EntryNote> entryNote = entryNoteService.findByHealthRecord(idHealthRecord);
              Iterable<DischargeNote> dischargeNotes = dischargeNoteService.findByHealthRecord(idHealthRecord);
              ArrayList<Prescription> findVitalSigns = new ArrayList<>();

              entryNote.forEach((x)-> findVitalSigns.add(x.getPrescription()));
              dischargeNotes.forEach((x)-> findVitalSigns.add(x.getPrescription())) ;
               return findVitalSigns;
          }





    @Override
    public Prescription updatePrescription(Prescription prescription) {
        return prescriptionRepository.save(prescription);
    }
}
