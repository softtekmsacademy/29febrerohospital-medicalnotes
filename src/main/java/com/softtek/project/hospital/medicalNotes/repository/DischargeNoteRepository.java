package com.softtek.project.hospital.medicalNotes.repository;

import com.softtek.project.hospital.medicalNotes.model.DischargeNote;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.stereotype.Repository;

@Repository
public interface DischargeNoteRepository extends CrudRepository<DischargeNote, Integer> {

}