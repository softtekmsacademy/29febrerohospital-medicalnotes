package com.softtek.project.hospital.medicalNotes.service;


import com.softtek.project.hospital.medicalNotes.exception.NoteNotFoundException;
import com.softtek.project.hospital.medicalNotes.model.PrescriptionDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.softtek.project.hospital.medicalNotes.repository.PrescriptionDetailRepository;
import java.util.ArrayList;
import java.util.Optional;


@Service
public class PrescriptionDetailServiceImpl implements PrescriptionDetailService {

    @Autowired
    PrescriptionDetailRepository prescriptionDetailRepository;

    @Override
    public Iterable<PrescriptionDetail> findByMedicalPrescription(Integer id) {
        Iterable<PrescriptionDetail> details = prescriptionDetailRepository.findAll();
        ArrayList<PrescriptionDetail> findNotes = new ArrayList<>();

        for (PrescriptionDetail detail:details){
            if(detail.getIdMedicalPrescriptionDetail().compareTo(id) == 0){
                findNotes.add(detail);
            }
        }

        return findNotes;
    }


    @Override
    public PrescriptionDetail updatePrescription(PrescriptionDetail prescription) {
        return prescriptionDetailRepository.save(prescription);
    }


}