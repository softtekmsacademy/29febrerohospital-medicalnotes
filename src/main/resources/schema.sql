
CREATE DATABASE medicalnotes;

USE medicalnotes;



--
-- Table structure for table `medical_prescription`
--

DROP TABLE IF EXISTS `medical_prescription`;
CREATE TABLE `medical_prescription` (
  `id_medical_prescription` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_medical_prescription`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Table structure for table `medical_prescription_detail`
--

DROP TABLE IF EXISTS `medical_prescription_detail`;
CREATE TABLE `medical_prescription_detail` (
  `id_medical_prescription_detail` int(11) NOT NULL AUTO_INCREMENT,
  `administration_route` varchar(255) DEFAULT NULL,
  `dose` varchar(255) DEFAULT NULL,
  `periodicity` varchar(255) DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL,
  `id_medical_prescription` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_medical_prescription_detail`),
  KEY `FKd0eu0bv64pc8o1o40r152xi2e` (`id_medical_prescription`),
  CONSTRAINT `FKd0eu0bv64pc8o1o40r152xi2e` FOREIGN KEY (`id_medical_prescription`) REFERENCES `medical_prescription` (`id_medical_prescription`)
) ENGINE=InnoDB AUTO_INCREMENT=255 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Table structure for table `vital_signs`
--

DROP TABLE IF EXISTS `vital_signs`;
CREATE TABLE `vital_signs` (
  `id_vital_signs` int(11) NOT NULL AUTO_INCREMENT,
  `blood_oxygen` float DEFAULT NULL,
  `blood_pressure` double DEFAULT NULL,
  `body_temperature` double DEFAULT NULL,
  `heart_rate` int(11) DEFAULT NULL,
  `mean_arterial_pressure` double DEFAULT NULL,
  `respiratory_rate` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_vital_signs`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;



-- Table structure for table `discharge_notes`
--

DROP TABLE IF EXISTS `discharge_notes`;
CREATE TABLE `discharge_notes` (
  `id_discharge_note` int(11) NOT NULL AUTO_INCREMENT,
  `creation_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `diagnosis` varchar(255) DEFAULT NULL,
  `id_employee` int(11) DEFAULT NULL,
  `id_health_record` int(11) DEFAULT NULL,
  `prognosis` varchar(255) DEFAULT NULL,
  `discharge_reason` varchar(255) DEFAULT NULL,
  `id_medical_prescription` int(11) DEFAULT NULL,
  `id_vital_signs` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_discharge_note`),
  KEY `FK5u4jvlqbk6utw0kiqwme6sh2k` (`id_medical_prescription`),
  KEY `FKoucv39recw9rg3ohkvmrdmpjk` (`id_vital_signs`),
  CONSTRAINT `FK5u4jvlqbk6utw0kiqwme6sh2k` FOREIGN KEY (`id_medical_prescription`) REFERENCES `medical_prescription` (`id_medical_prescription`),
  CONSTRAINT `FKoucv39recw9rg3ohkvmrdmpjk` FOREIGN KEY (`id_vital_signs`) REFERENCES `vital_signs` (`id_vital_signs`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


--
-- Table structure for table `entry_note`
--

DROP TABLE IF EXISTS `entry_note`;
CREATE TABLE `entry_note` (
  `id_entry_note` int(11) NOT NULL AUTO_INCREMENT,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `diagnosis` varchar(255) DEFAULT NULL,
  `id_employee` int(11) DEFAULT NULL,
  `id_health_record` int(11) DEFAULT NULL,
  `prognosis` varchar(255) DEFAULT NULL,
  `entry_reason` varchar(255) DEFAULT NULL,
  `id_medical_prescription` int(11) DEFAULT NULL,
  `id_vital_signs` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_entry_note`),
  KEY `FKraqwya09ob0yuw6ik2hdlrpda` (`id_medical_prescription`),
  KEY `FKgsfpmhatscru6rnsvu1259vm9` (`id_vital_signs`),
  CONSTRAINT `FKgsfpmhatscru6rnsvu1259vm9` FOREIGN KEY (`id_vital_signs`) REFERENCES `vital_signs` (`id_vital_signs`),
  CONSTRAINT `FKraqwya09ob0yuw6ik2hdlrpda` FOREIGN KEY (`id_medical_prescription`) REFERENCES `medical_prescription` (`id_medical_prescription`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


